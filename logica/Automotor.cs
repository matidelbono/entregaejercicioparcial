﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace logica
{
    class Automotor: Seguro
    {
        public int PatenteVehiculo { get; set; }
        public string Marca { get; set; }
        public int AnoDeFabricacion { get; set; }
        public bool EsMoto { get; set; }
        public int CantidadOcupantes { get; set; }

    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace logica
{
    class Incendio: Seguro
    {
        public int MetrosCuaddradosCasaAsegurada { get; set; }
        public int CantidadMatafuegos { get; set; }
        public int CantidadEnchufesElectricos { get; set; }
        public bool EsVivienda { get; set; }
        public decimal MontoBienesMateriales { get; set; }


    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace logica
{
    class Robo: Seguro
    {
        public int ModeloDelTelefono { get; set; }
        public int TamanoPantalla { get; set; }
        public decimal PrecioActual { get; set; }
        public DateTime FechaDeCompra { get; set; }

    }
}
